%?mingw_package_header

%define glib_ver 2.37.6

Summary:	MinGW build of Library for JavaScript Object Notation format
Name:		mingw-json-glib
Version:	1.2.2
Release:	1%{?dist}
License:	LGPLv2+
URL:		https://wiki.gnome.org/Projects/JsonGlib
Source0:	http://download.gnome.org/sources/%{name}/1.2/%{name}-%{version}.tar.xz

BuildArch:	noarch

BuildRequires:	mingw32-glib2 >= %{glib_ver}
BuildRequires:	mingw32-pkg-config
BuildRequires:	mingw32-filesystem >= 95
BuildRequires:	mingw32-gcc
BuildRequires:	mingw32-gcc-c++
BuildRequires:	mingw32-binutils

BuildRequires:	mingw64-glib2 >= %{glib_ver}
BuildRequires:	mingw64-pkg-config
BuildRequires:	mingw64-filesystem >= 95
BuildRequires:	mingw64-gcc
BuildRequires:	mingw64-gcc-c++
BuildRequires:	mingw64-binutils

BuildRequires:	docbook-style-xsl
BuildRequires:	gobject-introspection-devel
BuildRequires:	/usr/bin/xsltproc


%description
%{name} is a library providing serialization and deserialization support
for the JavaScript Object Notation (JSON) format.


# Win32
%package -n mingw32-json-glib
Summary:	MinGW build of Library for JavaScript Object Notation format

%description -n mingw32-json-glib
%{name} is a library providing serialization and deserialization support
for the JavaScript Object Notation (JSON) format.

%package -n mingw32-json-glib-static
Summary:	MinGW build of Library for JavaScript Object Notation format
Requires:	mingw32-json-glib = %{version}-%{release}

%description -n mingw32-json-glib-static
Static version of the MinGW Library for JavaScript Object Notation format

# Win64
%package -n mingw64-json-glib
Summary:	MinGW build of Library for JavaScript Object Notation format

%description -n mingw64-json-glib
%{name} is a library providing serialization and deserialization support
for the JavaScript Object Notation (JSON) format.

%package -n mingw64-json-glib-static
Summary:	MinGW build of Library for JavaScript Object Notation format
Requires:	mingw64-json-glib = %{version}-%{release}

%description -n mingw64-json-glib-static
Static version of the MinGW Library for JavaScript Object Notation format


%{?mingw_debug_package}


%prep
%setup -q -n json-glib-%{version}

iconv -f Latin1 -t UTF-8 -o ChangeLog.utf8 ChangeLog
mv ChangeLog.utf8 ChangeLog

%build
%mingw_configure --enable-static --enable-shared
%mingw_make %{?_smp_mflags} V=1


%install
%mingw_make_install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -name '*.la' -delete

rm -r ${RPM_BUILD_ROOT}%{mingw32_mandir}/man1/	# Duplicates native versions
rm -r ${RPM_BUILD_ROOT}%{mingw64_mandir}/man1/

%mingw_find_lang json-glib-1.0

# Win32
%files -n mingw32-json-glib -f mingw32-json-glib-1.0.lang
%doc NEWS
%license COPYING
%{mingw32_bindir}/json-glib-format.exe
%{mingw32_bindir}/json-glib-validate.exe
%{mingw32_bindir}/libjson-glib-1.0-0.dll
%{mingw32_includedir}/json-glib-1.0/
%{mingw32_libdir}/libjson-glib-1.0.dll.a
%{mingw32_libdir}/pkgconfig/json-glib-1.0.pc
#%{mingw32_libdir}/girepository-1.0/Json-1.0.typelib
#%{mingw32_datadir}/gir-1.0/Json-1.0.gir

%files -n mingw32-json-glib-static
%{mingw32_libdir}/libjson-glib-1.0.a

# Win64
%files -n mingw64-json-glib -f mingw64-json-glib-1.0.lang
%doc NEWS
%license COPYING
%{mingw64_bindir}/json-glib-format.exe
%{mingw64_bindir}/json-glib-validate.exe
%{mingw64_bindir}/libjson-glib-1.0-0.dll
%{mingw64_includedir}/json-glib-1.0/
%{mingw64_libdir}/libjson-glib-1.0.dll.a
%{mingw64_libdir}/pkgconfig/json-glib-1.0.pc
#%{mingw64_libdir}/girepository-1.0/Json-1.0.typelib
#%{mingw64_datadir}/gir-1.0/Json-1.0.gir

%files -n mingw64-json-glib-static
%{mingw64_libdir}/libjson-glib-1.0.a


%changelog
* Thu Sep 08 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.2.2-1
- Initial Fedora spec converted to MinGW.

