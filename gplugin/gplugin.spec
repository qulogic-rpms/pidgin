# Lua build is failing?
%global with_lua 1

Name:		gplugin
Version:	0.27.0
Release:	1%{?dist}
License:	LGPLv2+
Summary:	A GObject based library that implements a reusable plugin system
Url:		https://bitbucket.org/gplugin/main/overview
Group:		Development/Libraries
Source0:	https://bitbucket.org/%{name}/main/downloads/%{name}-%{version}.tar.bz2

BuildRequires:	cmake >= 2.8
BuildRequires:	glib2-devel >= 2.32.0
BuildRequires:	gobject-introspection-devel
BuildRequires:	gettext
BuildRequires:	gtk3-devel
BuildRequires:	help2man
BuildRequires:	libxslt
%if 0%{?with_lua}
BuildRequires:	lua-devel
BuildRequires:	lua-lgi
BuildRequires:	lua-moonscript
%endif # with_lua
BuildRequires:	python3-devel
BuildRequires:	python3-gobject
BuildRequires:	pygobject3-devel


%package devel
Summary:	A GObject based library that implements a reusable plugin system
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}

%package gtk3
Summary:	A GObject based library that implements a reusable plugin system
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}

%package gtk3-devel
Summary:	A GObject based library that implements a reusable plugin system
Group:		Development/Libraries
Requires:	%{name}-gtk3%{?_isa} = %{version}-%{release}
Requires:	%{name}-devel%{?_isa} = %{version}-%{release}

%if 0%{?with_lua}
%package lua
Summary:	A GObject based library that implements a reusable plugin system
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}
%endif # with_lua

%package python3
Summary:	A GObject based library that implements a reusable plugin system
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}


%description
GPlugin is a GObject based library that implements a reusable plugin system
that supports loading plugins in other languages via loaders.  GPlugin also
implements dependencies among the plugins.

It was started due to the infamous "Plugin Problem" for Guifications 3, which
was that I needed plugins that could depend on each other, be able to be
written in other languages, have plugins that are loaded before the main load
phase, and allow plugins to register types into the GObject type system.

%description devel
Development files for %{name} library.

%description gtk3
GTK3 files for %{name} library.

%description gtk3-devel
GTK3 Development files for %{name} library.

%if 0%{?with_lua}
%description lua
Lua loaders for %{name} library.
%endif # with_lua

%description python3
Python 3 loader for %{name} library.


%prep
%setup -q


%build
mkdir -p build
pushd build

CFLAGS="%{optflags}" cmake \
    -DCMAKE_INSTALL_PREFIX=/usr \
	-DCMAKE_INSTALL_LIBDIR=%{_lib} \
    ..

make %{?_smp_mflags}


%install
pushd build

make install DESTDIR=%{buildroot}

# Unneeded files
rm -rf %{buildroot}/usr/share/doc/gplugin/

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post gtk3 -p /sbin/ldconfig

%postun gtk3 -p /sbin/ldconfig


%files
%doc README ChangeLog
%license COPYING
%{_bindir}/gplugin-query
%{_libdir}/libgplugin.so.*
%dir %{_libdir}/gplugin
%{_libdir}/gplugin/gplugin-license-check.so
%{_libdir}/girepository-1.0/GPlugin-0.0.typelib
%{_datadir}/gir-1.0/GPlugin-0.0.gir
%{_mandir}/man1/gplugin-query.1*

%files devel
%doc README HACKING
%license COPYING
%{_includedir}/gplugin-1.0/
%{_libdir}/libgplugin.so
%{_libdir}/pkgconfig/gplugin.pc

%files gtk3
%doc README
%license COPYING
%{_bindir}/gplugin-gtk-viewer
%{_libdir}/libgplugin-gtk.so.*
%{_datadir}/gplugin/gplugin-gtk/
%{_mandir}/man1/gplugin-gtk-viewer.1*

%files gtk3-devel
%doc README
%license COPYING
%{_libdir}/libgplugin-gtk.so
%{_libdir}/pkgconfig/gplugin-gtk.pc

%if 0%{?with_lua}
%files lua
%doc README
%license COPYING
%{_libdir}/gplugin/gplugin-lua.so
%endif # with_lua

%files python3
%doc README
%license COPYING
%{_libdir}/gplugin/gplugin-python.so


%changelog
* Thu Sep 08 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.27.0-1
- New upstream release

* Wed Feb 17 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.25.0-1
- New upstream release

* Wed Nov 25 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.0.22-1
- New upstream release
- Fix incorrect split of files per package

* Wed May 13 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.0.20-2
- Correct all URLs
- Fix package division for shared libraries

* Wed May 13 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.0.20-1
- New upstream release
- Use a better source URL
- Re-enable Lua loader

* Mon May 4 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.0.18-1
- Initial package release
